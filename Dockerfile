# This file is a template, and might need editing before it works on your project.
FROM python:3.9.7

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.

# WORKDIR /usr/src/app

#ADD ./app

#COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

#COPY . /usr/src/app


# For some other command
CMD ["python", "app.py"]
